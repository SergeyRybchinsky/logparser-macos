//
//  main.cpp
//  LogParser
//
//  Created by Serge.Rybchinsky on 24/01/17.
//  Copyright © 2017 Serge.Rybchinsky. All rights reserved.
//

#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <regex>

using namespace std;

//test data
// /Users/sergerybchinsky/Desktop/

// JispiOSShareExtension

int main(int argc, const char * argv[]) {

    std::cout << "Hello!\n";
    
    std::cout << "Enter file path(or just d&d text file): ";
    string filePath;
    getline (cin, filePath);
    std::cout << "File path: " << filePath << "\n";
    
    std::cout << "Enter targer word: ";
    string targetWordForFinding;
    getline(cin, targetWordForFinding);
    std::cout << "\n";
    
    ifstream myfile (filePath);
    
    if (myfile.is_open())
    {
        std::ofstream outputFile(filePath+"O");
        
        string regexPatter = "\\d{1,2}[-\\:. ]\\d{1,2}[-\\:. ]\\d{1,4}";//fine data by 66:66:66 pattern - as is new line mark
        regex e (regexPatter);
        
        string line;
        bool foundNewLine = false;
        bool foundTarget = false;

        while ( getline (myfile,line) )
        {
            //if found new line
            foundNewLine = regex_search(line, e);
            if(foundNewLine)
            {
                //then we check if found tager word
                foundTarget = line.find(targetWordForFinding) != std::string::npos;
                if(foundTarget)
                {
                    //then outpute line
                    outputFile << line << endl;
                    cout << line << endl;
                }
            }
            //else if not found new line, but it line is target line
            else if (foundTarget)
            {
                //then outpute line
                outputFile << line << endl;
                cout << line << endl;
            } else
            {
                foundTarget = false;
            }
        }
        
        outputFile.close();
        myfile.close();
    }
    else cout << "Unable to open file.\n";
    
    std::cout << "job finished.\n\n";
    std::getchar();
    main(argc, argv);
    
    return 0;
}
